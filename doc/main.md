Indiegogo working with LibreJS
=============================

This project's goal is to make indiegogo usable when the LibreJS plugin is installed.

Users should be able to install LibreJS, and this plugin, visit indiegogo.com and then
search, browse and contribute to campaigns via the for-profit credit card partner (Stripe).

REMEMBER:
This add-on is doing several potentially dangerous things:
-> Intercepting pages you load
-> Parsing markup to get information about the pages you're visiting
-> Submitting credit card details to a third party (Stripe in this case)

WHAT THIS ADD-ON DOES NOT DO:
-> Store or save any data.
-> Pass any data on to anyone other than those who absolutely need it.

Copyright (C) 2014,2015 Ryan J. F. White
Entire project released under GNU GPL v3 or later
