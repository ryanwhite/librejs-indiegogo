/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */

/*
  This one does some basic cleanup of the page used to display information about the
  project and shepherds the user to the "contribute now" button.
*/


$(document).ready(function(){
    $("ul.i-perks").hide();
    $("div.js-nonperk-container").hide();
    $(".i-sidebar-header").hide();
    $("div.i-pitch-and-i-catch").parent().show();
    $(".i-contribute-button").text("Contribute Now");
    $("#i-contrib-box-rework").append($(".i-contribute-button").first());
    //hide the links to comments and stuff, until we cater for those too...
    $(".i-float-tab-links").hide();
});


if(location.href.indexOf("/new") == -1){
    $.ajax({
        type:"GET",
        beforeSend: function (request)
        {
            request.setRequestHeader("X-CSRF-Token",$("[name=authenticity_token]").first ().val())
        },
        url: location.href+"/show_tab/home",
        data: null,
        success: function(msg) {

            var sanObj = {html:msg};//sanitizer returns html - modified, and orig - original submitted
            self.port.emit('sanitize',sanObj);
            //once sanitize is finished, it'll broadcast to us
            self.port.on('sanitized',function(subsObj){
                switch(subsObj.action){
                case"alpha":
                    var msg = subsObj.html;
                    $(".i-campaign-body .i-main-column").first().empty();
                    //$(".i-campaign-body .i-main-column" ).first().append($(".i-make-it-happen").first());
                    //TODO- there should be a call to sanitize system
                    $(".i-campaign-body .i-main-column").first().append(msg);
                    $("div .i-pitch-and-i-catch").hide();
                    $("div .i-share").hide();
                    $("ul.i-perks").remove();
                    $("div.i-sidebar-header:contains('Select a Perk')").remove();
                    break;
                }
            });
        },
        error: function (jqXHR,textStatus,errorThrown){
        }
    });
}
