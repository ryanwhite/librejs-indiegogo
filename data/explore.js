/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */
/*
  The "explore" page is used to display the results of a user initiated search. In standard operation, the user types in a phrase, say "gnu",
  which then results in a redirection to a page like this:
  https://www.indiegogo.com/explore?filter_title=gnu#/search

  YES - user data is being sent to the page, which then retrieves data, based on the unsanitised variable. It seems to be how the system works.
*/

function page_init(){

    //typical get request:
    // https://www.indiegogo.com/private_api/explore?experiment=true&filter_funding=&filter_percent_funded=&filter_status=&locale=en&per_page=6&search_text=gnu
    //https://www.indiegogo.com/private_api/explore?experiment=true&filter_funding=&filter_percent_funded=&filter_status=&locale=en&per_page=6&search_text=technology
    //NOTE: yes - we're using an unsanitised url parameter - the same method indiegogo uses! This is no different to, for example, google, with a typical url of:
    //https://www.google.co.za/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=technology
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    }

    var filter_title = getURLParameter('filter_title');
    var url = "/private_api/explore?experiment=true&filter_funding=&filter_percent_funded&filter_status=&locale=en&per_page=100&search_text="+filter_title;

    $.ajax({
        type:"GET",
        beforeSend: function (request)
        {
       //     request.setRequestHeader("X-CSRF-Token",$("[name=authenticity_token]").first ().val())
        },
        url: url,
        data: null,
        success: function(msg) {
            var inserty_table_json = [];
            var inserty_json = [];
            inserty_json.push(['html:tr',{},['html:th',{}],['html:th',{},'Active']]);
            var inserty_expired_json = [];
            inserty_expired_json.push(['html:tr',{},['html:th',{}],['html:th',{},'Out of Time']]);
            //we're interested only in campaigns - we don't support life.indiegogo yet...
            for(var key in msg.campaigns){
                var link_style = "";
                if(msg.campaigns[key].amt_time_left == "No time left"){
                    link_style="color:#cccccc;";
                    inserty_expired_json.push(['html:tr',{},['html:td',{},['html:a',{href:msg.campaigns[key].url, style:link_style},['html:img',{src:msg.campaigns[key].compressed_image_url, width:'50', height:'50'},'']]],['html:td',{},['html:a',{href:msg.campaigns[key].url, style:link_style},msg.campaigns[key].title]]]);
                }else{

                    inserty_json.push(['html:tr',{},['html:td',{},['html:a',{href:msg.campaigns[key].url, style:link_style},['html:img',{src:msg.campaigns[key].compressed_image_url, width:'50', height:'50'},'']]],['html:td',{},['html:a',{href:msg.campaigns[key].url, style:link_style},msg.campaigns[key].title]]]);
                }
            }

            inserty_table_json.push(inserty_json);
            inserty_table_json.push(inserty_expired_json);

            $("[explore-header-www]").append(jsonToDOM(inserty_table_json, document, {}));
        },
        error: function (jqXHR,textStatus,errorThrown){
            alert("Sorry! we had an error:"+errorThrown);
        }
    });
}
page_init();
