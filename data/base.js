/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */

/*
  This one does basic setup on any page loaded from indiegogo. Basic hiding of stuff, as well as inserting of the search form.
*/
$(".i-home-featured-carousel").hide();
$(".js-homepage-video").hide();
$(".i-header-links.i-header-account-links").hide();

var searchjson =
    ['html:div', {style:'height:30px;border:solid 1px #00ff00; padding-left:1em;padding-right:1em;'},
     ['html:input', {type:'text',id: 'filter_title'}],
     ['html:button', {id:'explore-button',style:'font-weight:bold;'}, 'Search for a project'],
     ['html:span', {style:'float:right;line-height:30px;'}, 'Assisted by: ',
      ['html:a', {href:'https://addons.mozilla.org/en-US/firefox/addon/indiegogolibrejs/',target:'_blank'},'LibreJS+indiegogo']]];

$("body").prepend(jsonToDOM(searchjson, document, {}));

$("#explore-button").click(function(){
    location.href="/explore/?filter_title="+$("#filter_title").val();
});

//hide the life stuff - we don't support it yet, because it uses a non-profit payments processor, and we don't know how it works yet.
$(".i-home-ringfence").hide();
