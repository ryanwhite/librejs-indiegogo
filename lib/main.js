/** @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 *   Copyright (C) 2014,2015 Ryan J. F. White
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 *  @licend  The above is the entire license notice
 *  for the JavaScript code in this page.
 */

var data = require("sdk/self").data;
var pageMod = require("sdk/page-mod");
var {Cc, Ci} = require("chrome");

//logging level
let sp = require('sdk/simple-prefs');
sp.prefs['sdk.console.logLevel'] = 'info';//for debugging purposes

//These PageMod methods allow us to include scripts to modify and manipulate pages on loading.

//generic include - basically adds the search bar to all pages and hides some stuff we're not interested in.
pageMod.PageMod({
    include: "*.indiegogo.com",
    contentScriptWhen: "ready",
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url ("utils.js"),
                        data.url ("base.js")],
    onAttach:startListening
});


function startListening(worker) {
    worker.port.on('click', function(html) {
        worker.port.emit('warning', 'Do not click this again');
    });
    worker.port.on('sanitize', function(sanObj){
        //sanitising

          var parser = Cc["@mozilla.org/parserutils;1"].getService(Ci.nsIParserUtils);
        var sanitized = parser.sanitize(sanObj.html, parser.SanitizerAllowStyle);
        var ret = {html:sanitized, orig:sanObj.html, action:'alpha'};
        worker.port.emit('sanitized',ret);
    });
}


//Project page - info on the project. Basically just shows the "contribute now" button
pageMod.PageMod({
    include: /.*indiegogo\.com.*projects.*/,
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url ("utils.js"),
                        data.url ("projects.js")],
    contentScriptWhen: "ready"
});


//The contributions page, in which the user selects an amount and conducts a payment
//2016-03-07 - contributions seems to now be payment
pageMod.PageMod({
    include: /.*indiegogo\.com.*payments.*/,
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url("LibreStripe.js"),
                        data.url ("utils.js"),
                        data.url ("contributions.js")],
    onAttach:startListening
});

//explore - search results
pageMod.PageMod({
    include: /.*indiegogo\.com.*explore.*/,
    contentScriptWhen: "ready",
    contentScriptFile: [data.url("jquery-1.11.3.js"),
                        data.url ("utils.js"),
                        data.url ("explore.js")]
});
